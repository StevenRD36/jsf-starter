package controllers;

import backend.FacadeInterface;
import backend.PersonsDummy;
import domain.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PersonControllerTest {

    //what it should be
//    @Mock
//    private PersonFacade personFacade;

    //Injecting mocks is a technique used for dependency injection with mock testing
    @Mock
    private PersonsDummy personsDummy;

    @InjectMocks
    private PersonsController personsController;


    @Before
    public void init(){
//        personsController = new PersonsController();
    }

    @Test
    public void testInit(){

        List<User> users = new ArrayList<>(new PersonsDummy().findAll());
        when(personsDummy.findAll()).thenReturn(users);

        personsController.init();

        assertEquals(personsController.getUsers().size(),users.size());
        verify(personsDummy,times(1)).findAll();
    }


}
