package converters;

import backend.PersonsDummy;
import domain.User;
import domain.UserBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserConverterTest {

    @Mock
    private PersonsDummy personsDummy;
    @InjectMocks
    private UserConverter userConverter;

    @Test
    public void getAsObjectTest(){
        User user = new UserBuilder().setFirstName("Steven").setLastName("De Cock").build();
        when(personsDummy.findById(100)).thenReturn(user);

        User result = userConverter.getAsObject(null,null, "100");

        assertEquals(user.getFirstName(),result.getFirstName());
        verify(personsDummy,times(1)).findById(100);
    }

    @Test
    public void getAsObjectNotStringParamTest(){
        User result = userConverter.getAsObject(null,null, "100df");
        assertNull(result);
    }

    @Test
    public void getAsObjectParamNullTest(){
        User result = userConverter.getAsObject(null,null,null);
        assertNull(result);
    }

    @Test
    public void getAsObjectParamEmptyTest(){
        User result = userConverter.getAsObject(null,null,"");
        assertNull(result);
    }
}
