package controllers;

import backend.FacadeInterface;
import backend.PersonsDummy;
import com.ocpsoft.pretty.PrettyContext;
import domain.User;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;

@Named
@SessionScoped
public class RegistrationController implements Serializable {

    //dummy
    @Inject
    private PersonsDummy personsDummy;
    //real
    //@Inject private PersonFacade;
    private User user;

    private List<String> genders;
    private List<String> levels;
    private Boolean editable;

    @PostConstruct
    public void init() {
        if(user == null){
            user = new User();
            editable = true;
        }
        genders = new ArrayList<>();
        genders.add("Male");
        genders.add("Female");
        genders.add("X");
        levels = new ArrayList<>(Arrays.asList("Basic","Medium","High"));
        System.out.println("PersonsDummy " + personsDummy==null);
    }


    public String register() {
        this.editable = false;
        return "pretty:register";
    }

    public String edit(){
        this.editable = true;
        return "pretty:register";
    }

    public void save(){
        personsDummy.add(user);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<String> getGenders() {
        return genders;
    }

    public void setGenders(List<String> genders) {
        this.genders = genders;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public List<String> getLevels() {
        return levels;
    }

    public void setLevels(List<String> levels) {
        this.levels = levels;
    }
}
