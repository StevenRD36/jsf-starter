package controllers;

import domain.User;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author SDOAX36
 */
@Named
@ViewScoped
public class ConfirmController implements Serializable {
    
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    
}
