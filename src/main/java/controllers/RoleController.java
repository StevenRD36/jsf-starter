
package controllers;

import backend.RoleDummy;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author SDOAX36
 */
@Named
@ViewScoped
public class RoleController implements Serializable{
    
    @Inject
    private RoleDummy roleDummy;
    
    private List<String> roles;
    private String role;
    
    @PostConstruct
    public void init(){
        roles = roleDummy.getRoles();
    }
    
    public void addRole(){
        if(role!=null&&!role.trim().isEmpty()){
            roleDummy.addRole(role);
            roles = roleDummy.getRoles();
            role = "";
        }
    }
    
    public void deleteRole(String dRole){
        roleDummy.deleteRole(dRole);
        roles = roleDummy.getRoles();
    }

    public RoleDummy getRoleDummy() {
        return roleDummy;
    }

    public void setRoleDummy(RoleDummy roleDummy) {
        this.roleDummy = roleDummy;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
    
    
}
