package controllers;

import backend.FacadeInterface;
import backend.PersonsDummy;
import domain.User;
import domain.UserBuilder;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;

@Named
@ViewScoped
public class PersonsController implements Serializable {

    private List<User> users;
    private String search;
    private List<User>filteredUsers;

    //As long as our real backend is not present as a dependency we use the dummy
    @Inject
    private PersonsDummy personsDummy;

    //This is what it really should be
    //@Inject private PersonFacade personFacade;

    @PostConstruct
    public void init() {
        System.out.println("In init method");
        //dummy
        users = personsDummy.findAll();
        filteredUsers = new ArrayList<>(users);
        //real
//        users = personFacade.findAll();
    }

    public void filter(){
        if(search!=null&&!search.trim().isEmpty()){
            filteredUsers = personsDummy.filter(search.trim());
        }
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public List<User> getFilteredUsers() {
        return filteredUsers;
    }

    public void setFilteredUsers(List<User> filteredUsers) {
        this.filteredUsers = filteredUsers;
    }
   
}
