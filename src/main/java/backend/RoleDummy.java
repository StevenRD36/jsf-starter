
package backend;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author SDOAX36
 */
@ApplicationScoped
public class RoleDummy implements Serializable{

    private List<String> roles;
    
    public RoleDummy() {
        roles = new ArrayList<>(Arrays.asList("Admin","Reader","Deleter","Jelter"));
    }

    public List<String> getRoles() {
        return roles;
    }

    
    public void addRole(String role){
        roles.add(role);
    }
    
    public void deleteRole(String role){
        if(roles.contains(role)){
            roles.remove(role);
        }
    }
    
}
