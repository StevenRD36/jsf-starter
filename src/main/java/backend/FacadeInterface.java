package backend;

import java.util.List;

public interface FacadeInterface<C,T> {

    List<C> findAll();
    C findById(T id);
    void add(C object);
    //more can be added
}
