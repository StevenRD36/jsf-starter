package backend;

import domain.User;
import domain.UserBuilder;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Month;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Named;

/**
 * PersonsDummy will mimic my backend
 * in my controllers I can easily replace PersonsDummy with for example PersonsFacade
 * I will create the same methods in my Dummy as I have in my Facade,
 * I will also add some tests to prove that you can work more efficiently this way.
 */

@ApplicationScoped()
public class PersonsDummy implements FacadeInterface<User,Integer>, Serializable{

    private List<User> users;

    public PersonsDummy() {
        createUsers();
    }


    @PostConstruct
    public void init(){
        createUsers();
    }

    private void createUsers(){

        users = new ArrayList<>();
        for(int i = 1; i<20; i++){
            users.add(new UserBuilder().setId(i)
                    .setFirstName("First Name"+i)
                    .setLastName("Last Name"+i)
                    .setDateOfBirth(new Date())
                    .setGender(i%2==0?"Male":"Female").build());
        }
    }

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User findById(Integer id) {
        System.out.println("IN find by id"+id);
        User user = users.stream().filter(u->u.getId().intValue() == id.intValue()).findFirst().get();
        System.out.println("Found a user "+user.getFirstName());
        return user;
    }

    @Override
    public void add(User object) {
        this.users.add(object);
    }
    
    public List<User> filter(String search){
        return users.stream()
                .filter(u-> u.getLastName().toLowerCase().contains(search.toLowerCase()) 
                        || u.getFirstName().toLowerCase().contains(search.toLowerCase()))
                .collect(Collectors.toList());
    }
}
