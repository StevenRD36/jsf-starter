package domain;

/**
 *
 * @author SDOAX36
 */
public class AccountNumber {
    private String prefix,number,suffix;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @Override
    public String toString() {
        return prefix + "-" + number + "-" + suffix;
    }
    
    
    
}
