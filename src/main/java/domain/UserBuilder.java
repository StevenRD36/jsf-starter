package domain;

import java.time.LocalDate;
import java.util.Date;

public class UserBuilder {

    private User user;

    public UserBuilder(){
        user = new User();
    }

    public UserBuilder setId(Integer id){
        this.user.setId(id);
        return this;
    }

    public UserBuilder setFirstName(String fn){
        this.user.setFirstName(fn);
        return this;
    }
    public UserBuilder setLastName(String last){
        this.user.setLastName(last);
        return this;
    }
    public UserBuilder setDateOfBirth(Date dateOfBirth){
        this.user.setDateOfBirth(dateOfBirth);
        return this;
    }
    public UserBuilder setGender(String gender){
        this.user.setGender(gender);
        return this;
    }

    public User build() {
        return this.user;
    }
}
