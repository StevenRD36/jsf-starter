
package converters;

import backend.PersonsDummy;
import domain.AccountNumber;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

/**
 *
 * @author SDOAX36
 */
@FacesConverter(forClass = AccountNumber.class)
public class AccountNumberConverter implements Converter{

    @Inject
    private PersonsDummy personsDummy;
    
    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        return o.toString();
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value== null || value.trim().equals("")) {
            return null;
        }
        
        if(personsDummy == null){
            System.out.println("shit it's null");
        }
        
        String[] split = value.split("-");
        if (split.length == 3) {
            AccountNumber accountNumber = new AccountNumber();
            accountNumber.setPrefix(split[0]);
            accountNumber.setNumber(split[1]);
            accountNumber.setSuffix(split[2]);
            return accountNumber;
        } else {

            throw new ConverterException(new FacesMessage("Not able to convert this accountnumber"));
        }    
    }

}
