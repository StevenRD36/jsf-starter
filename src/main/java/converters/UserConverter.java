package converters;

import backend.PersonsDummy;
import domain.User;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

/**
 * @author SDOAX36
 */
@FacesConverter(value = "userConverter",managed = true)
public class UserConverter implements Converter<User> {

    @Inject
    private PersonsDummy personsDummy;

    //@Inject private PersonsFacade personsFacade;

    @Override
    public User getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.trim().isEmpty()) {
            return null;
        }
        if (personsDummy == null) {
            return null;
        }
        try {
            Integer id = Integer.valueOf(value);
            return personsDummy.findById(id);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, User value) {

        return value != null ? value.getId().toString() : "";
    }
}
