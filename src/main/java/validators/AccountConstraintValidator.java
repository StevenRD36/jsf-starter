package validators;

import domain.AccountNumber;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigInteger;

public class AccountConstraintValidator implements ConstraintValidator<Account,AccountNumber> {

    @Override
    public void initialize(Account constraintAnnotation) {

    }

    @Override
    public boolean isValid(AccountNumber value, ConstraintValidatorContext context) {
        if(value == null){
            return false;
        }
        BigInteger number = new BigInteger(value.getPrefix()+value.getNumber());

        BigInteger checksum = new BigInteger(value.getSuffix());
        BigInteger resultMod = number.mod(BigInteger.valueOf(97));
        return checksum.equals(resultMod);
    }
}
