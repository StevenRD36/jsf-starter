
package validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author SDOAX36
 */
public class EmailValidator implements ConstraintValidator<Email, String>{

    @Override
    public boolean isValid(String t, ConstraintValidatorContext cvc) {
        return checkEmail(t); //To change body of generated methods, choose Tools | Templates.
    }
    
        public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private boolean checkEmail(String emailStr) {
        if (emailStr == null || emailStr.equals("")) {
            return false;
        }
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }
    
    
}
